<?php header('Access-Control-Allow-Origin: *'); ?>
<?php
//session_start();
//$base_url = "http://localhost/simx";
$base_url = "https://www.simx.tv";

$code = $_REQUEST['code'];
$url = urlencode('https://web.simx.tv/linkedin_authentication.php');
// header("Location: https://www.linkedin.com/oauth/v2/accessToken?client_id=86ld41cejbmt12&client_secret=jIQKHKfWKMOSYgQT&redirect_uri=$url&grant_type=authorization_code&code=$code");

$curl = curl_init();
curl_setopt($ch, CURLOPT_CAINFO, 'C:/cacert.pem');
curl_setopt($ch, CURLOPT_CAPATH, 'C:/cacert.pem');
curl_setopt_array($curl, array(
    CURLOPT_URL => "https://www.linkedin.com/oauth/v2/accessToken?client_id=86ld41cejbmt12&client_secret=jIQKHKfWKMOSYgQT&redirect_uri=$url&grant_type=authorization_code&code=$code",
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => "",
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 30,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => "GET",
    CURLOPT_HTTPHEADER => array(
        "cache-control: no-cache",
        "postman-token: 383ac4b1-55bc-b707-164e-4fdebf6485bc",
    ),
));

$response = curl_exec($curl);
$err = curl_error($curl);

curl_close($curl);

if ($err) {
    echo "cURL Error #:" . $err;
} else {
    $response = json_decode($response);

    $access_token = $response->access_token;

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => "https://api.linkedin.com/v2/me",
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "authorization: Bearer $access_token",
            "cache-control: no-cache",
            "postman-token: 071e577c-e52f-a5fd-1252-514325cfa4e8",
        ),
    ));

    $response = curl_exec($curl);
    $err = curl_error($curl);

    curl_close($curl);

    if ($err) {
        echo "cURL Error #:" . $err;
    } else {
        $response = json_decode($response);
        $id = $response->id;
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_PORT => "90",
            CURLOPT_URL => "http://www.simx.tv:90/api/v2/mysql/_table/users/$id",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "postman-token: dacf1d89-7cd2-ba69-0e2d-60546587b5bc",
                "x-dreamfactory-api-key: b0a363a985c7a2ddf5057789adad5fe0dd47c8a38eaee82a963e3d3d6353ed1e",
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response);
            
            if($response->error->code == 404){
               header("Location: $base_url/notRegistered.html"); 
            } 
            else{
                $username = $response->username;
                header("Location: $base_url/home.php?username=$username");
            }        
        }
    }

}
exit;
?>