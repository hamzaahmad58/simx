<?php
// Required if your environment does not handle autoloading
require __DIR__ . '/vendor/autoload.php';

// Use the REST API Client to make requests to the Twilio REST API
use Twilio\Rest\Client;

// Your Account SID and Auth Token from twilio.com/console
$sid = 'ACb40200fc49eacdafdfae7135bf7519ea';
$token = '0fcedb079fce41eb59b76611d97c9eae';
$client = new Client($sid, $token);

if (isset($_POST["phoneNumber"]) && isset($_POST["body"])){

$phoneNumber = $_POST["phoneNumber"];
$body = $_POST["body"];

foreach ($phoneNumber as $key => $number) {
try{

$client->messages->create(
    // the number you'd like to send the message to
    $number,
    array(
        // A Twilio phone number you purchased at twilio.com/console
        'from' => '+13124710229 ',
        // the body of the text message you'd like to send
        'body' => $body
    )
);
}catch(Exception $e){
echo json_encode(array("status"=>false, "message"=>$e->getMessage()));
}
}
} 
else
{
	echo json_encode(array("status"=>false, "message"=>"Phone Number or Message not valid"));
	
}

// Use the client to do fun stuff like send text messages!
