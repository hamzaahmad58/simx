<?php

require "init.php";

?>
<!DOCTYPE html>
<html lang="eng">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Linkedin Posting</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<style type="text/css" media="screen">
			body, html {
				height: 100%;
			}
			.bg {
				height: 100;
				background-image: url('bg.jpg');
				background-repeat: no-repeat;
				background-position: center;
				background-size: cover;
			}
		</style>
	</head>
	<body class="bg">

		<div class="container">
			<br><br><br>
			<div class="row">
				<div class="col-6 offset-6" style="margin: auto; background: white; padding: 28px; box-shadow: 10px 10px 5px #888; ">
					<div class="panel-heading">
						<h1>Share On Linkedin</h1>
						<p style="font-style: italic;">Using Linkedin API v2</p>
					</div>
					<hr>
					<div class="panel-body">
						<a href="<?php echo $linkedin->getAuthUrl(); ?>" title="" class="btn btn-primary">Sign In With Linkedin</a>
					</div>
				</div>
			</div>
		</div>
		<!-- jQuery -->
		<script src="https://code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	
	</body>
</html>