<?php

$type = $_GET['type'];

switch ($type) {
	case 'text':
		header('Location: gettextpost.php');
		break;
	case 'link':
		header('Location: getlinkpost.php');
		break;
	case 'image':
		header('Location: getimagepost.php');
		break;
	
	default:
		die('INVALID USE OF HEADER INFORMATION');
		break;
}