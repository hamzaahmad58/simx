<?php

require "init.php";

if ($_GET['state'] != $_SESSION['linkedincsrf']) {
	die("INVALID REQUEST");
}

$code = $_GET['code'];
$accessToken = $linkedin->getAccessToken($code);

if (!$accessToken) {
	die("NO ACCESS TOKEN FOUND");
}

$_SESSION['linkedinAccessToken'] = $accessToken;

header('Location: profile.php');