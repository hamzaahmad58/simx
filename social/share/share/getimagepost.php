<?php

require "init.php";
$profile = $linkedin->getPerson($_SESSION['linkedinAccessToken']);

// echo "<pre>";
// print_r($profile);
// exit;

?>

<!DOCTYPE html>
<html lang="eng">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>LinkedIn Posting</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
		<style type="text/css" media="screen">
			body, html {
				height: 100%;
			}
			.bg {
				height: 100;
				background-image: url('bg.jpg');
				background-repeat: no-repeat;
				background-position: center;
				background-size: cover;
			}
		</style>
	</head>
	<body class="bg">

		<div class="container">
			<br><br><br>
			<div class="row">
				<div class="col-3 offset-6" style="margin: auto; background: white; padding: 28px; box-shadow: 10px 10px 5px #888; ">
					<div class="panel-heading">
						<h1>Share On Linkedin</h1>
						<p style="font-style: italic;">Profile</p>
					</div>
					<hr>
					<div class="panel-body">
						<div class="row">
							<div class="col-3">
								<img src="<?php echo $profile->profilePicture->{"displayImage~"}->elements[0]->identifiers[0]->identifier; ?>" alt="user" class="thumbnail" >
							</div>
							<div class="col-9">
								<dl class="row">
									<dt class="col-12">
									Profile ID:	
									</dt>
									<dd class="col-12">
									<?php echo $profile->id; ?>	
									</dd>	
								</dl>

								<dl class="row">
									<dt class="col-12">
									Profile Name:	
									</dt>
									<dd class="col-12">
									<?php echo $profile->firstName->localized->en_US." ".$profile->lastName->localized->en_US; ?>	
									</dd>	
								</dl>
								
							</div>
						</div>

						<hr>
						<h5>Share Post</h5>
						<form action="getimagepost.php" method="get" accept-charset="utf-8">
							<select name="type" id="type" class="form-control" required>
								<option value="">Select post type</option>
								<option value="text">Text Post</option>
								<option value="link">Link POst</option>
								<option value="image">Image Post</option>
							</select>
							<br>
							<input type="submit" class="btn btn-danger btn-block" value="Proceed">
						</form>
					</div>
				</div>
			</div>
		</div>

		<!-- jQuery -->
		<script src="https://code.jquery.com/jquery.js"></script>
		<!-- Bootstrap JavaScript -->
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
	</body>
</html>