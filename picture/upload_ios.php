<?php
define('UPLOAD_DIR', './Photos/');
// function resizeImage($resourceType, $image_width, $image_height)
// {
//     $resizeWidth = 10;
//     $resizeHeight = 10;
//     $imageLayer = imagecreatetruecolor($resizeWidth, $resizeHeight);
//     imagecopyresampled($imageLayer, $resourceType, 0, 0, 0, 0, $resizeWidth, $resizeHeight, $image_width, $image_height);
//     return $imageLayer;
// }

$img = $_REQUEST['base64'];
$name = $_REQUEST['ImageName'];
$img = str_replace('data:image/png;base64,', '', $img);
$img = str_replace(' ', '+', $img);
$data = base64_decode($img);
$file = UPLOAD_DIR . $name;
$success = file_put_contents($file, $data);

// $sourceProperties = getimagesize($file);

// $uploadImageType = $sourceProperties[2];
// $sourceImageWidth = $sourceProperties[0];
// $sourceImageHeight = $sourceProperties[1];

// $resourceType = imagecreatefrompng($file);
// $imageLayer = resizeImage($resourceType, $sourceImageWidth, $sourceImageHeight);
// imagepng($imageLayer, UPLOAD_DIR . 'web_' . $name);

print $success ? $file : 'Unable to save the file.';
