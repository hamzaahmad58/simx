<?php

$srcFile = "fb.mp4";
$destFile = "Sample22";
$ffmpegPath = "ffmpeg.exe";
//$flvtool2Path = "/usr/local/bin/flvtool2";

// Create our FFMPEG-PHP class
$ffmpegObj = new ffmpeg_movie($srcFile);
// Save our needed variables
$srcWidth = makeMultipleTwo($ffmpegObj->getFrameWidth());
$srcHeight = makeMultipleTwo($ffmpegObj->getFrameHeight());
$srcFPS = $ffmpegObj->getFrameRate();
$srcAB = intval($ffmpegObj->getAudioBitRate()/1000);
$srcAR = $ffmpegObj->getAudioSampleRate();
$srcVB = floor($ffmpegObj->getVideoBitRate()/1000); 

// Call our convert using exec() to convert to the three file types needed by HTML5
exec($ffmpegPath . " -i ". $srcFile ." -vcodec libx264 -vpre hq -vpre ipod640 -b ".$srcVB."k -bt 100k -acodec libfaac -ab " . $srcAB . "k -ac 2 -s " . $srcWidth . "x" . $srcHeight . " ".$destFile.".mp4");

exec($ffmpegPath . " -i ". $srcFile ." -vcodec libvpx -r ".$srcFPS." -b ".$srcVB."k -acodec libvorbis -ab " . $srcAB . " -ac 2 -f webm -g 30 -s " . $srcWidth . "x" . $srcHeight . " ".$destFile.".webm");

exec($ffmpegPath . " -i ". $srcFile ." -vcodec libtheora -r ".$srcFPS." -b ".$srcVB."k -acodec libvorbis -ab " . $srcAB . "k -ac 2 -s " . $srcWidth . "x" . $srcHeight . " ".$destFile.".ogv");

?>