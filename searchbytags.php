<?php 
$servername = "localhost:3307";
$username = "root";
$password = "Lisaco563";
$dbname = "bitnami_df";

// Create connection
$conn = mysqli_connect($servername, $username, $password, $dbname);
/* check connection */
if (mysqli_connect_errno()) {
    printf("Connect failed: %s\n", mysqli_connect_error());
    exit();
}

$search = $_GET['search'];
// sql to create table
$sql = "SELECT broadcasts.*, tags.tag, GROUP_CONCAT(tags.tag ORDER BY broadcasts.broadcast ASC SEPARATOR ',') AS tags FROM `broadcasts` LEFT  JOIN tags ON tags.broadcast=broadcasts.broadcast WHERE title like '%$search%' or name like '%$search%' or location like '%$search%' or tags.tag like '%$search%' GROUP by broadcasts.broadcast";



$result = mysqli_query($conn, $sql);

if (mysqli_num_rows($result) > 0) { 
    $output = array();
  // output data of each row
  while($row = mysqli_fetch_assoc($result)) {
      if($row['broadcast'] != null){ 
          $tags = array();
          $list = explode(",",$row['tags']);
          foreach($list as $l){
              $tags[]= array(
                  "tag" => $l,
                "broadcast" => $row['broadcast']
              );
          }
          
          // getting user details
          $user = mysqli_query($conn, "select * from users where username='$row[username]'");
          $user = mysqli_fetch_assoc($user);
          
          // getting jobcandidate details
          $candidates = mysqli_query($conn, "select * from jobcandidates where broadcast_id='$row[id]'");
          $candidates_list = array();
          while($c = mysqli_fetch_assoc($candidates)) {
              $candidates_list[] = $c;
          }
          
          $output[]= array(
            "username" => $row['username'],
             "broadcast" => $row['broadcast'],
            "location" => $row['location'],
            "latti" => $row['latti'],
            "longi" => $row['longi'],
            "imglink" => $row['imglink'],
            "id" => $row['id'],
            "title" => $row['title'],
            "skill" => $row['skill'],
            "status" => $row['status'],
            "arn" => $row['arn'],
            "name" => $row['name'],
            "time" => $row['time'],
            "viewers" => $row['viewers'],
            "isOffline" => $row['isOffline'],
            "isJob" => $row['isJob'],
            "tags_by_broadcast" => $tags,
            "users_by_username" => $user,
            "jobcandidates_by_broadcast" => $candidates_list
          );
      }
  } 
    echo json_encode(array("status" => "success", "resource" => $output));
} else {
  echo json_encode(array("status" => "failure", "message" => "no data found"));
}

mysqli_close($conn);
?>