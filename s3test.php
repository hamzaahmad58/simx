<?php
//echo "<pre>";
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
require './PHP-FFMpeg-master/vendor/autoload.php';

if ($_SERVER['REQUEST_METHOD'] == "POST") {
    ini_set('memory_limit', '1G');
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
    $videolink = $_REQUEST['videoLink'];
    $imgName = $_REQUEST['imageName'];

    // if (exec_enabled()) {echo "yes";} else {echo "no";}
    if (!empty($videolink)) {
        set_time_limit(5000);

        $ffmpeg = FFMpeg\FFMpeg::create(array(
            'ffmpeg.binaries' => 'C:/ffmpeg/bin/ffmpeg.exe',
            'ffprobe.binaries' => 'C:/ffmpeg/bin/ffprobe.exe',
            'timeout' => 3600, // The timeout for the underlying process
            'ffmpeg.threads' => 12, // The number of threads that FFMpeg should use
        ));

        $thumbSize = '450x800';
        $second = 1;
        $image = './picture/Photos/' . $imgName . '.png';
        //        echo "C:/ffmpeg/bin/ffmpeg -i $videolink -vf fps=1 -s $thumbSize -vcodec png -f image2 -an -y -vframes 1 $image";
        exec("C:/ffmpeg/bin/ffmpeg -i $videolink -vf fps=1 -s $thumbSize -vcodec png -f image2 -an -y -vframes 1 $image");


        // $thumbSize = '45x80';
        // $second = 1;
        // $image = './picture/Photos/bg_' . $imgName . '.png';
        // //        echo "C:/ffmpeg/bin/ffmpeg -i $videolink -vf fps=1 -s $thumbSize -vcodec png -f image2 -an -y -vframes 1 $image";
        // exec("C:/ffmpeg/bin/ffmpeg -i $videolink -vf fps=1 -s $thumbSize -vcodec png -f image2 -an -y -vframes 1 $image");

        $msg = 'Thumbnail is created!';
        $status = true;
        $ar = array('message' => $msg, 'status' => $status);
        echo json_encode($ar);

        //        echo $msg;

    } else {
        $msg = 'Thumbnail is not created!';
        $status = false;
        $ar = array('message' => $msg, 'status' => $status);
        echo json_encode($ar);
        //echo "Empty name";
    }
} else {
    $ar = array('message' => "Method not allowed", 'status' => "error");
    echo json_encode($ar);
}

function exec_enabled()
{
    $disabled = explode(',', ini_get('disable_functions'));
    return !in_array('exec', $disabled);
}
