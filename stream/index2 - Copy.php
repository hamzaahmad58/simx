<!DOCTYPE html>
<?php
include('functions.php');
$v = $_REQUEST['v'];
$myVideo = 'mp4:' . $v . '.mp4';
$p = $v;
$img = $p . '.png';
$status = get_broadcasts($v);
$application = "vod";
if ($status == "online") {
    $application = "live";
    $myVideo = $v;
}
//  echo $status;exit;              
?>

<html prefix="og: http://ogp.me/ns#" lang="en">

<head>
    <link href="https://unpkg.com/video.js/dist/video-js.min.css" rel="stylesheet">
    <script src="https://unpkg.com/video.js/dist/video.min.js"></script>
    <meta property="og:image:type" content="https://www.simx.tv/images/h2startup.png" />
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="https://www.simx.tv/stream/index2.php?v=<?php echo $p ?>">
    <meta name="twitter:creator" content="@MohsinAyoob">
    <meta name="twitter:title" content="H2Startup">
    <meta name="twitter:description" content="I am LIVE in SimX.">
    <meta name="twitter:image" content="https://www.simx.tv/picture/Photos/<?php echo $img ?>">

    <!-- facebook meta -->
    <meta property="fb:app_id" content="157300111470623" />
    <!-- <meta property="og:type" content="article">
    <meta property="og:title" content="SimX Livestream" />
    <meta property="og:description" content="I am LIVE in SimX. Aleem don" />
    <meta property="og:image" content="https://web.simx.tv/picture/Photos/<?php echo $img ?>" />
    <meta property="og:url" content="https://web.simx.tv/stream/index2.php?v=<?php echo $p ?>" />
    <meta property="og:video" content="https://web.simx.tv/stream/index2.php?v=<?php echo $p ?>" />
    <meta property="og:video:width" content="480" />
    <meta property="og:video:height" content="720" />
    <meta property="og:video:secure_url" content="https://web.simx.tv/uploads/clips/<?php echo $v ?>.mp4" />
    <meta property="og:image:secure_url" content="https://web.simx.tv/picture/Photos/<?php echo $img ?>" /> --> -->

    <meta property="og:site_name" content="H2Startup">
    <meta property="og:title" content="H2Startup" />
    <meta property="og:description" content="Apply Online for a Job" />
    <meta property="og:image" itemprop="image" content="https://web.simx.tv/picture/h2startup.png">
    <meta property=" og:type" content="website" />
    <meta property="og:updated_time" content="1440432930" />




    <meta property="og:image:type" content="image/h2startup.png" />
    <meta property="og:image:width" content="480">
    <meta property="og:image:height" content="720">


    <link rel="stylesheet" href="./bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" href="./bootstrap/css/bootstrap.css">
    <link rel="icon" href="./images/h2startup.png">
    <script src='https://www.simx.tv/stream/js/jwplayer.js'></script>
    <script src="./js/jquery-3.1.0.js"></script>
    <title>H2Startup</title>
    <style>
        .ad_div {
            position: absolute;
            /* top: 50px; */
            /* left: 20%; */
            z-index: 21;
            height: 460px;
            width: 720px;
            padding-left: 18%;
            padding-top: 5%;
            background: rgba(0, 0, 0, 0.5);
        }

        @media screen and (max-width: 992px) {
            .ad_div {
                padding-left: 25%;
            }
        }
    </style>
</head>

<body>



    <header class="container-fluid " style="background-color:#04856f; text-align:center;">
        <!--  Change in img and link
-->

        <a href='https://apps.apple.com/us/app/SimX/id1387163430' style="float:right; color: white;text-decoration:None; margin-right:10px; padding-top:15px; padding-right:25px;" title="Get App"><img src="./images/Apple_app_store_icon.png" style="height:65px;width:200px;" alt="Google Play"></a>

        <h1 style="text-align: left; padding-top:15px;padding-bottom:15px;">
            <img src="./images/splash.png" style="height:65px; margin-top:0px; margin-right:20px;" alt="Logo Imag">
        </h1>
    </header>

    <div class="container">
        <div class="row row-centered">
            <div class="col-md-12 col-centered" style="margin:20px; ">
                <?php include('./ad.php'); ?>
                <video id='Video' class='video-js vjs-big-play-centered' controls preload='auto' width='720' height='480' poster='https://www.simx.tv/picture/Photos/<?php echo $img ?>' data-setup='{}'>
                    <source src="http://www.simx.tv:1935/<?php echo $application ?>/<?php echo $myVideo ?>/playlist.m3u8" type='application/x-mpegURL'>

                </video>
            </div>

        </div>

    </div>
    <script>
        var player = videojs('Video');
        player.on('timeupdate', function(e) {
            if (player.currentTime() >= 30) {
                player.pause();
                start_add();
                $('.ad_div').show();
            }
        });
        player.play();

        start_add()
        $('.ad_div').hide();
    </script>
</body>

</html>