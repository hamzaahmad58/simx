<!DOCTYPE html>
<?php
include('functions.php');
$v = $_REQUEST['v'];
$myVideo = 'mp4:' . $v . '.mp4';
$p = $v;
$img = $p . '.png';
$bd = get_broadcasts($v);
$status = $bd->{'status'};
$jobDesc = $bd->{'jobDescription'};
$application = "vod";
if ($status == "online") {
    $application = "live";
    $myVideo = $v;
}
if ($_REQUEST['type'] == "offline") {
    $videoLink = "https://simx.s3-us-west-2.amazonaws.com/offlineVideos/" . $v . ".mp4";
    $img = $v . '.png';
}
if ($_REQUEST['type'] == "recorded") {
    $videoLink = "https://simx.s3-us-west-2.amazonaws.com/recordedvideos/" . $v . ".mp4";
    $img = $v . '.png';
}


// $conn = new mysqli("localhost:3307", "root", "Lisaco563", "bitnami_df");
// if ($conn->connect_error) {
//     die("Connection failed: " . $conn->connect_error);
// }
// $sql = "SELECT * FROM `broadcasts` WHERE broadcast = '$v'";
// $result = $conn->query($sql);
// print_r($result);
// if ($result->num_rows > 0) {
//     while ($row = $result->fetch_assoc()) {
//         echo '<div><h3>Job Description</h3><span>' . $row["jobDescription"] . '</span></div>';
//     }
// } else {
//     echo "0 results";
// }

//  echo $status;exit;              
?>

<html prefix="og: http://ogp.me/ns#" lang="en">

<head>
    <link href="https://unpkg.com/video.js/dist/video-js.min.css" rel="stylesheet">
    <script src="https://unpkg.com/video.js/dist/video.min.js"></script>

    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="https://h2startup.com/stream/index2.php?v=<?php echo $p ?>">
    <meta name="twitter:creator" content="@MohsinAyoob">
    <meta name="twitter:title" content="H2Startup">
    <meta name="twitter:description" content="I am LIVE in SimX.">
    <meta name="twitter:image" content="https://h2startup.com/picture/h2startup.png">

    <!-- facebook meta -->
    <meta property="fb:app_id" content="157300111470623" />

    <meta property="og:site_name" content="H2Startup">
    <meta property="og:title" content="H2Startup" />
    <meta property="og:description" content="Apply Online for a Job" />
    <meta property="og:image" itemprop="image" content="https://h2startup.com/picture/h2startup.png">
    <meta property="og:type" content="website" />
    <meta property="og:url" content="https://h2startup.com/stream/index2.php?v=<?php echo $p ?>" />
    <meta property="og:updated_time" content="1440432930" />




    <meta property="og:image:width" content="480">
    <meta property="og:image:height" content="720">

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="stylesheet" href="./bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" href="./bootstrap/css/bootstrap.css">
    <link rel="icon" href="./images/h2startup.png">
    <script src='https://h2startup.com/stream/js/jwplayer.js'></script>
    <script src="./js/jquery-3.1.0.js"></script>
    <title>H2Startup</title>
    <style>
        .ad_div {
            position: absolute;
            /* top: 50px; */
            /* left: 20%; */
            z-index: 21;
            height: 460px;
            width: 720px;
            padding-left: 18%;
            padding-top: 5%;
            background: rgba(0, 0, 0, 0.5);
        }


        .for-img-card {
            width: 100%;
        }

        @media screen and (max-width: 992px) {
            .ad_div {
                padding-left: 25%;
            }
        }
    </style>
</head>

<body>



    <header class="container-fluid " style="background-color:#04856f; text-align:center;">
        <!--  Change in img and link
-->

        <a href='https://apps.apple.com/us/app/SimX/id1387163430' target="_blank" style="float:right; color: white;text-decoration:None; margin-right:10px; padding-top:15px; padding-right:25px;" title="Get App"><img src="./images/Apple_app_store_icon.png" style="height:65px;width:200px;" alt="Google Play"></a>


        <a href='https://play.google.com/store/apps/details?id=com.senarios.simxx' target="_blank" style="float:right; color: white;text-decoration:None; margin-right:10px; padding-top:15px; padding-right:25px;" title="Get App"><img src="./images/Google_app_store_icon.jpg" style="height:65px;width:200px;" alt="Google Play"></a>


        <h1 style="text-align: left; padding-top:15px;padding-bottom:15px;">
            <img src="./images/h2startup.png" style="height:65px; margin-top:0px; margin-right:20px;" alt="Logo Imag">
            <p style="  font-size: 22px;
    float: right;
    color: white;margin-right:15px;margin-top:20px;">Download the app and video chat with me.</p>
        </h1>
    </header>

    <div class="container">
        <div class="row row-centered">
            <div class="col-md-12 col-centered" style="margin:20px; ">
                <?php include('./ad.php'); ?>
                <video id='Video' class='video-js vjs-big-play-centered' controls preload='auto' width='720' height='480' poster='https://h2startup.com/picture/Photos/<?php echo $img ?>' data-setup='{}'>
                    <!--                <source src="http://www.simx.tv:1935/<?php echo $application ?>/<?php echo $myVideo ?>/playlist.m3u8" type='application/x-mpegURL'>-->
                    <?php
                    if ($_REQUEST['type'] == "offline" || $_REQUEST['type'] == "recorded") {
                    ?>
                        <source src="<?php echo $videoLink ?>" type="video/mp4" />

                    <?php
                    } else { ?>
                        <source src="https://5ec654d8d930c.streamlock.net:1936/<?php echo $application ?>/<?php echo $myVideo ?>/playlist.m3u8" type="application/x-mpegURL" />
                    <?php
                    }
                    ?>

                </video>

                <div style="padding-top:1%;">
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-lg-3 col-sm-3 text-right">
                                            <img class="" src="h2StartupLogo.png" alt="sans" width="100px">
                                        </div>
                                        <div class="col-lg-9 col-sm-9" style="text-align: center; align-self:center">
                                            <h5 class="card-title" style="font-size: 20px;">Please Follow the Link</h5>
                                            <p class="card-text">
                                                <?php
                                                echo '<a style="font-size:20px;" href="' . $jobDesc . '" target="_blank">' ?> <?php echo $jobDesc ?></a></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        var player = videojs('Video');
        player.on('timeupdate', function(e) {
            //    if (player.currentTime() >= 30) {
            //        player.pause();
            //        start_add();
            //        $('.ad_div').show();
            //    }
        });
        player.play();

        start_add()
        $('.ad_div').hide();
    </script>
</body>

</html>