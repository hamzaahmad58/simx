<!DOCTYPE html>
<?php
include('functions.php');
$v = $_REQUEST['q'];
$myVideo = 'mp4:' . $v . '.mp4';
$p = $_REQUEST['q'];
$img = $p . '.png';
$status = get_broadcasts($v);
$application = "vod";
if ($status == "online") {
    $application = "live";
    $myVideo = $v;
}
if ($v == "offline") {
    $videoLink = "https://simx.s3-us-west-2.amazonaws.com/offlineVideos/" . $_REQUEST['i'] . ".mp4";
    $img = $_REQUEST['i'] . '.png';
}
//  echo $status;exit;              
?>

<html prefix="og: http://ogp.me/ns#" lang="en">

<head>
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:site" content="https://h2startup.com/stream/index.php?q=<?php echo $v ?>&i=<?php echo $p ?>">
    <meta name="twitter:creator" content="@MohsinAyoob">
    <meta name="twitter:title" content="H2Startup">
    <meta name="twitter:description" content="I am LIVE in H2Startup.">
    <meta name="twitter:image" content="https://h2startup.com/picture/Photos/<?php echo $img ?>">
    <!-- facebook meta -->
    <meta property="fb:app_id" content="157300111470623" />
    <meta property="og:type" content="article">
    <meta property="og:title" content="H2Startup" />
    <meta property="og:description" content="I am LIVE in H2Startup." />
    <meta property="og:image" content="https://h2startup.com/picture/Photos/<?php echo $img ?>" />
    <meta property="og:url" content="https://h2startup.com/stream/index.php?q=<?php echo $v ?>&i=<?php echo $p ?>" />
    <meta property="og:video" content="https://h2startup.com/stream/index.php?q=<?php echo $v ?>&i=<?php echo $p ?>" />
    <meta property="og:video:width" content="400" />
    <meta property="og:video:height" content="400" />
    <meta property="og:video:secure_url" content="https://h2startup.com/uploads/clips/<?php echo $v ?>.mp4" />
    <meta property="og:image:secure_url" content="https://h2startup.com/picture/Photos/<?php echo $img ?>" />


    <meta property="og:image:type" content="image/png" />
    <meta property="og:image:width" content="400">
    <meta property="og:image:height" content="400">



    <link rel="stylesheet" href="./bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" href="./bootstrap/css/bootstrap.css">
    <link rel="icon" href="./images/h2startup.png">
    <script src='https://h2startup.com/stream/js/jwplayer.js'></script>
    <script src="./js/jquery-3.1.0.js"></script>
    <title>SimX</title>
    <style>
        .ad_div {
            position: absolute;
            /* top: 50px; */
            /* left: 20%; */
            z-index: 21;
            height: 460px;
            width: 720px;
            padding-left: 18%;
            padding-top: 5%;
            background: rgba(0, 0, 0, 0.5);
        }

        @media screen and (max-width: 992px) {
            .ad_div {
                padding-left: 25%;
            }
        }
    </style>
</head>

<body>



    <header class="container-fluid " style="background-color:#04856f; text-align:center;">
        <!--  Change in img and link
-->

        <a href='https://apps.apple.com/us/app/SimX/id1387163430' target="_blank" style="float:right; color: white;text-decoration:None; margin-right:10px; padding-top:15px; padding-right:25px;" title="Get App"><img src="./images/Apple_app_store_icon.png" style="height:65px;width:200px;" alt="Google Play"></a>


        <a href='https://play.google.com/store/apps/details?id=com.senarios.simxx' target="_blank" style="float:right; color: white;text-decoration:None; margin-right:10px; padding-top:15px; padding-right:25px;" title="Get App"><img src="./images/Google_app_store_icon.jpg" style="height:65px;width:200px;" alt="Google Play"></a>

        <h1 style="text-align: left; padding-top:15px;padding-bottom:15px;">
            <img src="./images/splash.png" style="height:65px; margin-top:0px; margin-right:20px;" alt="Logo Imag">
        </h1>
    </header>

    <div class="container">
        <div class="row row-centered">
            <div class="col-md-12 col-centered" style="margin:20px; ">
                <?php include('./ad.php'); ?>


                <link href="./player/video-js.min.css" rel="stylesheet">
                <script src="./player/video.min.js"></script>
                <script src="./player/videojs-flash.min.js"></script>
                <script src="./swfobject/swfobject.js"></script>
                <script src="./player/iphone-inline-video.min.js"></script>
                <script src="https://unpkg.com/videojs-contrib-hls/dist/videojs-contrib-hls.js"></script>
                <style>
                    video.bkg {
                        background: black
                    }
                </style>
                <script>
                    $('#Video').on('loadstart', function(event) {
                        $(this).addClass('bkg');
                        $(this).attr("poster", "https://media.giphy.com/media/ycfHiJV6WZnQDFjSWH/giphy.gif");
                    });
                    $('#Video').on('canplay', function(event) {
                        $(this).removeClass('bkg');
                        $(this).removeAttr("poster");
                    });
                    //            $(document).ready(function() {
                    //                $document.getElementsByTagName('Video')[0].play();
                    $document.getElementById('Video')[0].play();
                    //$('#Video').prop('muted',true).play()
                    // });
                    //                $('#video').prop('muted',true)[0].play();
                </script>

                <a style="display:none" id="flash_link" href="https://www.adobe.com/go/getflashplayer/" class="button"><img src="./flash.jpg" width="500px" height="300px" /> </a>

                <video style="display:block" id="Video" class="video-js vjs-default-skin vjs-big-play-centered" preload="auto" width="720px" height="460px" playsinline poster="https:/h2startup.com/picture/Photos/<?php echo $img ?>" data-setup='{"techOrder": ["flash", "html5"], "nativeControlsForTouch": false, "controlBar": { "muteToggle": false, "volumeControl": false, "timeDivider": false, "durationDisplay": true, "progressControl": true } }' controls>



                    <!--    http://www.simx.tv:1935/vod/0HF9aisyT215625079714344/manifest.mpd-->/

                    <!--    <source src="http://www.simx.tv:1935/<?php echo $application ?>/mp4:<?php echo $myVideo ?>/playlist.m3u8" type='application/x-mpegURL'/>-->
                    <?php
                    if (false) {
                    ?>
                        <source src="<?php echo $videoLink ?>" type="video/mp4" />
                    <?php
                    } else { ?>
                        <source src="https://5ec654d8d930c.streamlock.net:1936/live/607320330/playlist.m3u8" type="application/x-mpegURL" />
                    <?php
                    }
                    ?>
                    <!--    <source src="http://www.simx.tv:1935/<?php echo $application ?>/<?php echo $myVideo ?>/playlist.m3u8" type="application/x-mpegURL"/>-->

                </video>

            </div>

        </div>

    </div>
    <script src="https://vjs.zencdn.net/ie8/ie8-version/videojs-ie8.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/videojs-contrib-hls/5.14.1/videojs-contrib-hls.js"></script>
    <script src="https://vjs.zencdn.net/7.2.3/video.js"></script>
    <script>
        var player = videojs('Video');
        player.on('timeupdate', function(e) {
            if (player.currentTime() >= 30) {
                player.pause();
                start_add();
                $('.ad_div').show();
            }
        });
        player.play();

        start_add()
        $('.ad_div').hide();
    </script>
    <script>
        /*
        $(document).ready(function(){
        jwplayer('video_container').setup({
        'id': 'playerID',
    'image':'https://www.cyberscope.tv/picture/Photos/<?php echo $img ?>',
    'width': '720',
    'height': '460',
    'provider': 'rtmp',
    'streamer': 'rtmp://www.cyberscope.tv:1935/<?php echo $application ?>/mp4:<?php echo $myVideo ?>',
    'file':"<?php echo $myVideo ?>",
	
    'modes': [
        
      {type: 'flash', src: 'https://www.cyberscope.tv/stream/js/player.swf'},
      {
        type: 'html5',
        config: {
          'file': 'https://www.cyberscope.tv:1935/<?php echo $application ?>/mp4:<?php echo $myVideo ?>/playlist.m3u8',
          'provider': 'video',
            'image':'https://www.cyberscope.tv/picture/Photos/<?php echo $img ?>'
        }
      },
      {
        type: 'download',
        config: {
			
          'file': 'rtsp://www.cyberscope.tv:1935/<?php echo $application ?>/<?php echo $myVideo ?>',
          'provider': 'video',
            //image: "./123.png"
        }
      }
    ]
  });
        });*/
    </script>
    <script>
        function flashAlive() {
            flashEnabled = true;
        }

        function detectFlashPlayerInstalled() {
            //TODO: detect flash in a better way, this will do for now
            return swfobject.getFlashPlayerVersion();
        }

        function myTimer() {
            console.log(' each 1 second...');
            var hasFlash = detectFlashPlayerInstalled();
            var flashEnabled = false;

            /* attempt to load the swf file then wait for some time, then... */

            var isDisabled = hasFlash && !flashEnabled;
            if (hasFlash.major != 0) {
                var x = document.getElementById("flash_link");
                var y = document.getElementById("Video");
                x.style.display = "none";
                y.style.display = "block";
                clearInterval(myVar);
            } else {
                var x = document.getElementById("flash_link");
                x.style.display = "block";
                var y = document.getElementById("Video");
                y.style.display = "none";
            }
        }

        //var myVar = setInterval(myTimer, 3000);
        var el = document.getElementById("Video");
        swfobject.embedSWF("test.swf", el, 300, 120, 10);
    </script>
</body>

</html>