<?php

session_start();

//if (isset($_POST['submit'])) {

// get user data
$username = $_POST['username'];
$session_id = "";

$data = array('resource' => array('username' => $username));
$data_json = json_encode($data);

$ch = curl_init("localhost:90/api/v2/mysql/_table/users");

curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
curl_setopt($ch, CURLOPT_HTTPHEADER, array(
    "Content-Type: application/json",
    "X-DreamFactory-API-Key: b0a363a985c7a2ddf5057789adad5fe0dd47c8a38eaee82a963e3d3d6353ed1e",
    "X-DreamFactory-Session-Token: $session_id"
));

$result = curl_exec($ch);

//echo "$result";
//print_r(json_decode($result));
//exit;
$arr = json_decode($result);
$data = $arr->resource[0];
$name = $data->name;
$bank = $data->bank_name;
$account = $data->account_no;
$emaill_status = $data->email_status;
$emaill = $data->email;
$cred = $data->credit;
$payment = $data->payment_status;
$ibann = $data->iban;
$sort_codee = $data->sort_code;
$bicc = $data->bic;
$phone_noo = $data->phone_no;
$payeee_name = $data->payee_name;
$pending_cred = $data->pending_credit;
$num = 1;

if ($emaill_status == $num) {

    $datta = array(
        'data' => array(
            'username' => $username, 'bank_name' => $bank, 'account_no' => $account, 'credit' => $cred, 'pending_credit' => $pending_cred,
            'msg' => "Your payment of " . $pending_cred . " credits is pending approval by your inapp payment provider.", 'email_status' => $emaill_status,
            'payment_status' => $payment, 'iban' => $ibann, 'sort_code' => $sort_codee, 'bic' => $bicc, 'phone_no' => $phone_noo,
            'payee_name' => $payeee_name
        )
    );

    $datta_json = json_encode($datta);

    echo $datta_json;
} else {

    $bank_name = $_POST['bank_name'];
    $account_no = $_POST['account_no'];
    $pending_credit = $_POST['pending_credit'];
    $iban = $_POST['iban'];
    $sort_code = $_POST['sort_code'];
    $bic = $_POST['bic'];
    $phone_no = $_POST['phone_no'];
    $payee_name = $_POST['payee_name'];
    $payment_status = "pending";
    $zero = 0;
    //echo $pending_credit;
    //exit();

    //
    //update userdata
    $session_id = "";

    $data = array('resource' => array(
        'username' => $username,
        'bank_name' => $bank_name, 'account_no' => $account_no,
        'pending_credit' => $pending_credit, 'credit' => $zero,
        'email_status' => $num, 'payment_status' => $payment_status,
        'iban' => $iban,
        'bic' => $bic, 'sort_code' => $sort_code, 'phone_no' => $phone_no, 'payee_name' => $payee_name
    ));

    $data_json = json_encode($data);

    $ch = curl_init("localhost:90/api/v2/mysql/_table/users");

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "X-DreamFactory-API-Key: b0a363a985c7a2ddf5057789adad5fe0dd47c8a38eaee82a963e3d3d6353ed1e",
        "X-DreamFactory-Session-Token: $session_id"
    ));

    $result = curl_exec($ch);


    require 'vendor/autoload.php';
    require_once 'confiq.php';


    $email = new \SendGrid\Mail\Mail();
    $email->setFrom("hamzachaudhry58@gmail.com", "hamza");
    $email->setSubject("Payment Verification");
    $email->addTo("colinjohn563@gmail.com", "Example User");
    $email->addContent("text/html", "<b>Users infromation is as following:</b> <br><br> <b>username:</b> " . $username .
        "<br> <b>email:</b> " . $emaill . "<br> <b>bank:</b> " . $bank_name . "<br><b>account number:</b> " . $account_no .
        "<br> <b>pending credit:</b> " . $pending_credit . "<br> <b>bic:</b> " . $bic . "<br> <b>iban:</b> " . $iban .
        "<br> <b>phone no:</b> " . $phone_no . "<br> <b>sort code:</b> " . $sort_code . "<br> <b>payee_name:</b> " . $payee_name);

    $sendgrid = new \SendGrid(SENDGRID_API_KEY);
    try {
        $response = $sendgrid->send($email);
        //    print $response->statusCode() . "\n";
        //  print_r($response->headers());
        //print $response->body() . "\n";
    } catch (Exception $e) {
        //echo 'Caught exception: ' . $e->getMessage() . "\n";
    }

    //
    // get user data
    $session_id = "";

    $data = array('resource' => array('username' => $username));
    $data_json = json_encode($data);

    $ch = curl_init("localhost:90/api/v2/mysql/_table/users");

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "X-DreamFactory-API-Key: b0a363a985c7a2ddf5057789adad5fe0dd47c8a38eaee82a963e3d3d6353ed1e",
        "X-DreamFactory-Session-Token: $session_id"
    ));

    $result = curl_exec($ch);

    $arr = json_decode($result);
    $data = $arr->resource[0];
    $emaill_status = $data->email_status;
    $payment = $data->payment_status;
    $ibann = $data->iban;
    $sort_codee = $data->sort_code;
    $bicc = $data->bic;
    $phone_noo = $data->phone_no;
    $payeee_name = $data->payee_name;
    $pending_cred = $data->pending_credit;
    $cred = $data->credit;

    $datta = array(
        'data' => array(
            'username' => $username, 'bank_name' => $bank_name, 'account_no' => $account_no,
            'credit' => $cred, 'pending_credit' => $pending_cred,
            'msg' => "Your transaction of " . $pending_cred . " credits has been successfully completed. Payment to your account will be made within 45 working days .",
            'email_status' => $emaill_status, 'payment_status' => $payment, 'iban' => $ibann, 'sort_code' => $sort_codee, 'bic' => $bicc,
            'phone_no' => $phone_noo, 'payee_name' => $payeee_name
        )
    );

    $datta_json = json_encode($datta);

    echo $datta_json;
}
//}
