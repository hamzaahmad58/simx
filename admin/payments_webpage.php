<html>

<head>
    <title>Payments</title>

    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://kit.fontawesome.com/9208a7dee1.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/call_history.css">
</head>

<body>
    <header>
        <div class="container-fluid" style="background-color:#04856f; width: 100%;height: auto;">
            <div class="row">
                <div class="col-md-4">
                    <a href="#"> <img src="/stream/images/splash.png" style="height: 40px; width: auto; margin-left: 60px; margin-top:15px; "></a>
                </div>
                <div class="col-md-8">
                    <div class="nav text-center">
                        <a class="btn btn-nav" href='./delete.php' role="button"><i class="fas fa-home"></i> Home</a>
                        <a class="btn btn-nav" href='./user.php' role="button"><i class="fas fa-user-friends"></i> User</a>
                        <a class="btn btn-nav" href='./payments_webpage.php' role="button"><i class="fas fa-money-bill-alt"></i> Payments</a>
                        <a class="btn btn-nav" href='./call_history.php' role="button"><i class="fas fa-phone-square-alt"></i> Call History</a>
                        <a class="btn btn-nav" href='./logout.php' role="button"><i class="fas fa-sign-out-alt"></i> Logout</a>
                    </div>
                </div>

                <br><br><br>
            </div>

        </div>
    </header>

    <div class="container">
        <div class="call-log">
            <br><br>
            <h3>Payment Status <sup><i class="fas fa-info-circle" title="Pending payment status is shown here"></i></sup></h3>
            <br><br><br>
            <?php
            $divCOunt = 0;
            $im = "";
            $br = "";
            include('conn.php');

            // get user data
            $email_statuss = 1;
            $session_id = "";

            $ch = curl_init("localhost:90/api/v2/mysql/_table/users?filter=email_status%20%3D%20$email_statuss");

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            //curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                "Content-Type: application/json",
                "X-DreamFactory-API-Key: b0a363a985c7a2ddf5057789adad5fe0dd47c8a38eaee82a963e3d3d6353ed1e",
                "X-DreamFactory-Session-Token: $session_id"
            ));

            $result = curl_exec($ch);
            $arr = json_decode($result);
            $data = $arr->resource;

            ?>
            <table id="example" class="table table-striped table-bordered" style="width:100%">
                <thead>
                    <tr>
                        <th>Username</th>
                        <th>Name</th>
                        <th>Pending Credit</th>
                        <th>Options</th>
                        <th>Status</th>
                    </tr>
                </thead>

                <?php if (sizeof($data) != 0) {
                    $size = sizeof($data);
                    for ($i = 0; $i < sizeof($data); $i++) { ?>
                        <tr>
                            <td> <?php echo $data[$i]->{'username'} ?> </td>
                            <td> <?php echo $data[$i]->{'name'} ?> </td>
                            <td> <?php echo $data[$i]->{'pending_credit'} ?> </td>
                            <td><a href="payments_status.php?name=<?php echo $data[$i]->{'username'} ?>&status=approved"><input class="btn btn-success" type="submit" name="Accepted" value="Approve"></a>
                                <a href="payments_status.php?name=<?php echo $data[$i]->{'username'} ?>&status=unapproved"><input class="btn btn-danger" type="submit" name="Denied" value="Unapprove"></a></td>
                            <td><?php echo $data[$i]->{'payment_status'} ?></td>
                        </tr>

                <?php  }
                } ?>
                <thead>
                    <tr>
                        <th>Username</th>
                        <th>Name</th>
                        <th>Pending Credit</th>
                        <th>Options</th>
                        <th>Status</th>
                    </tr>
                </thead>

            </table>
            <br><br><br><br><br>
        </div>
    </div>

    <script type="text/javascript" src="js/call_history.js"></script>

</body>

<script type="text/javascript">
    $(document).ready(function() {
        $('#example').DataTable();

    });
</script>

</html>