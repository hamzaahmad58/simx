<?php

function getdata()
{
    $session_id = "";


    $ch = curl_init("localhost:90/api/v2/mysql/_table/broadcasts");

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "X-DreamFactory-API-Key: b0a363a985c7a2ddf5057789adad5fe0dd47c8a38eaee82a963e3d3d6353ed1e",
        "X-DreamFactory-Session-Token: $session_id"
    ));

    $result = curl_exec($ch);

    //echo "$result";
    return json_decode($result);
}
function getcallsData()
{
    $session_id = "";


    $ch = curl_init("localhost:90/api/v2/mysql/_table/calls_records");

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "X-DreamFactory-API-Key: b0a363a985c7a2ddf5057789adad5fe0dd47c8a38eaee82a963e3d3d6353ed1e",
        "X-DreamFactory-Session-Token: $session_id"
    ));

    $result = curl_exec($ch);

    //echo "$result";
    return json_decode($result);
}

function getuserData()
{
    $session_id = "";


    $ch = curl_init("localhost:90/api/v2/mysql/_table/users");

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);


    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "X-DreamFactory-API-Key: b0a363a985c7a2ddf5057789adad5fe0dd47c8a38eaee82a963e3d3d6353ed1e",
        "X-DreamFactory-Session-Token: $session_id"
    ));

    $result = curl_exec($ch);

    //echo "$result";
    return json_decode($result);
}
