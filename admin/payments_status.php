<?php

if (isset($_GET['status']) && ($_GET['name'])) {
    // echo $_GET['status'];
} else {
}
$username = $_GET['name'];
$status = $_GET['status'];
$eml_status = 0;
$zero = 0;
$approved = "approved";
$uapproved = "unapproved";

if ($status == "approved") {
    $session_id = "";

    $data = array('resource' => array(
        'username' => $username, 'payment_status' => $approved,
        'email_status' => $eml_status, 'pending_credit' => $zero
    ));
    $data_json = json_encode($data);

    $ch = curl_init("localhost:90/api/v2/mysql/_table/users");

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "X-DreamFactory-API-Key: b0a363a985c7a2ddf5057789adad5fe0dd47c8a38eaee82a963e3d3d6353ed1e",
        "X-DreamFactory-Session-Token: $session_id"
    ));

    $result = curl_exec($ch);

    header('location:payments_webpage.php');
} else if ($status == "unapproved") {
    // get user data
    $session_id = "";

    $data = array('resource' => array('username' => $username));
    $data_json = json_encode($data);

    $ch = curl_init("localhost:90/api/v2/mysql/_table/users");

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "X-DreamFactory-API-Key: b0a363a985c7a2ddf5057789adad5fe0dd47c8a38eaee82a963e3d3d6353ed1e",
        "X-DreamFactory-Session-Token: $session_id"
    ));

    $result = curl_exec($ch);

    $arr = json_decode($result);
    $data = $arr->resource[0];
    $pending_cred = $data->pending_credit;
    $cred = $data->credit;
    $sum;

    $sum = $cred + $pending_cred;
    //    echo $cred;
    //   echo $pending_cred;
    //  echo $sum;
    // exit();
    $session_id = "";

    $data = array('resource' => array(
        'username' => $username, 'payment_status' => $approved,
        'email_status' => $eml_status, 'credit' => $sum, 'pending_credit' => $zero
    ));
    $data_json = json_encode($data);

    $ch = curl_init("localhost:90/api/v2/mysql/_table/users");

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        "Content-Type: application/json",
        "X-DreamFactory-API-Key: b0a363a985c7a2ddf5057789adad5fe0dd47c8a38eaee82a963e3d3d6353ed1e",
        "X-DreamFactory-Session-Token: $session_id"
    ));

    $result = curl_exec($ch);
    curl_close($ch);
    echo curl_error($ch);
    //echo $result;
    header('location:payments_webpage.php');
}
