<?php
session_start(); // Starting Session

if (isset($_SESSION["user"]) == false) {
    header('Location:./index.php');
}

if (isset($_REQUEST["logout"]) == true) {
    unset($_SESSION["user"]);
    header('Location:./index.php');
}
?>
<html>
<link rel="icon" href="./images/splash.png">
<title>SimX Admin Panel</title>

<head>
    <link rel="stylesheet" href="./bootstrap/css/bootstrap-theme.css">
    <link rel="stylesheet" href="./bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="fancybox/source/jquery.fancybox.css">


    <!--	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	-->
    <script src="https://kit.fontawesome.com/9208a7dee1.js"></script>
    <link rel="stylesheet" type="text/css" href="./css/nav.css">


    <script type='text/javascript' src='https://jwplayer.mediaserve.com/jwplayer.js'></script>
    <script src="./js/jquery-3.1.0.js"></script>
    <link href="./css/delete.css">
    <script src="fancybox/source/jquery.fancybox.js"></script>
</head>







<body>
    <header>
        <div class="container-fluid" style="background-color:#04856f; width: 100%;height: auto;">
            <div class="row">
                <div class="col-md-4">
                    <a href="#"> <img src="/stream/images/splash.png" style="height: 40px; width: auto; margin-left: 60px; margin-top:15px; "></a>
                </div>
                <div class="col-md-8">
                    <div class="nav text-center">
                        <a class="btn btn-nav" href='./delete.php' role="button"><i class="fas fa-home"></i> Home</a>
                        <a class="btn btn-nav" href='./user.php' role="button"><i class="fas fa-user-friends"></i> User</a>
                        <a class="btn btn-nav" href='./payments_webpage.php' role="button"><i class="fas fa-money-bill-alt"></i> Payments</a>
                        <a class="btn btn-nav" href='./call_history.php' role="button"><i class="fas fa-phone-square-alt"></i> Call History</a>
                        <a class="btn btn-nav" href='./logout.php' role="button"><i class="fas fa-sign-out-alt"></i> Logout</a>
                    </div>
                </div>

                <br><br><br>
            </div>

        </div>
    </header>
    <!-- <header class="container-fluid " style="background-color:#04856f; align:center;" >
                   
          <a href='./logout.php' style="float:right; color: white;text-decoration:None; margin:10px;"title="Logout"><i class="fa fa-sign-out fa-3x" aria-hidden="true" style="font-color:white;"></i></a>
        

        <h1>
      <img src="/stream/images/splash.png" style="height:60px;width:250px; margin-right:20px;">
            </h1>
        </header>-->
    <div class="row well">
        <div class="col-md-12 well">

            <?php
            $divCOunt = 0;
            $im = "";
            $br = "";
            include('conn.php');
            $result = getdata();
            $result = array_reverse($result->resource, true);
            //            echo "<pre>";
            //            print_r($result);exit;
            //            $result = array_reverse($result);
            if (sizeof($result) != 0) {
                $size = sizeof($result);
                for ($i = 0; $i < sizeof($result); $i++) {
                    $divCount++;
                    if ($divCount == 1) {
                        echo '<div class="row well">';
                    }
                    if ($i == 0) {
                        $i = 1;
                    }
                    $im = (string)$result[$size - $i]->{'imglink'};
                    $br = (string)$result[$size - $i]->{'broadcast'};
                    $type = ($result[$size - $i]->{'isOffline'}) ? "offline" : "recorded";
                    echo '<div class="col-md-2" onclick="divClick(this.id,' . "'$im'" . ',' . "'$br'" . ', true,' . "'$type'" . ');" id="' . $result[$size - $i]->{'id'} . '"><img src=../picture/Photos/' . $result[$size - $i]->{'imglink'} . '.png style="height:200px;width:200px; padding:20px;">';
                    echo "<br> <strong>id:</strong> " . $result[$size - $i]->{'id'} . " <br><strong>Name:</strong> " . $result[$size - $i]->{'username'} . "   <br><strong>Title: </strong>" . "<br>" . $result[$size - $i]->{'title'} . "<br></div>";
                    if ($divCount == 6) {
                        echo '</div>';
                        $divCount = 0;
                    }
                }
            } else {
                echo "0 results";
            }


            ?>


        </div>


    </div>
</body>
<script type='text/javascript' src="./js/system3.js">
</script>

</html>