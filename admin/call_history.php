<?php
session_start(); // Starting Session

if (isset($_SESSION["user"]) == false) {
  header('Location:./index.php');
}

if (isset($_REQUEST["logout"]) == true) {
  unset($_SESSION["user"]);
  header('Location:./index.php');
}
?>
<html>

<head>
  <title>Call History</title>

  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <script src="https://kit.fontawesome.com/9208a7dee1.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

  <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
  <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
  <link rel="stylesheet" type="text/css" href="css/call_history.css">
</head>

<body>
  <header>
    <div class="container-fluid" style="background-color:#04856f; width: 100%;height: auto;">

      <div class="row">
        <div class="col-md-4">
          <a href='./delete.php'> <img src="img/splash.png" style="height: 40px; width: auto; margin-left: 60px; margin-top:15px; "></a>
        </div>
        <div class="col-md-8" style="width: 100%;">
          <div class="nav text-center">
            <a class="btn btn-nav" href='./delete.php' role="button"><i class="fas fa-home"></i> Home</a>
            <a class="btn btn-nav" href='./user.php' role="button"><i class="fas fa-user-friends"></i> User</a>
            <a class="btn btn-nav" href='./payments_webpage.php' role="button"><i class="fas fa-money-bill-alt"></i> Payments</a>
            <a class="btn btn-nav" href='./call_history.php' role="button"><i class="fas fa-phone-square-alt"></i> Call History</a>
            <a class="btn btn-nav" href='./logout.php' role="button"><i class="fas fa-sign-out-alt"></i> Logout</a>
          </div>
        </div>

        <br><br><br>
      </div>

    </div>
  </header>

  <div class="container">
    <div class="call-log">
      <br><br>
      <h3>Call Logs <sup><i class="fas fa-info-circle" title="Call History Has Been Saved Here For Your Guaidence"></i></sup></h3>
      <br><br><br>
      <?php
      $divCOunt = 0;
      $im = "";
      $br = "";
      include('conn.php');
      $result = getcallsData();
      $result = array_reverse($result->resource, true);
      //            echo "<pre>";
      // print_r($result[0]->{'caller_name'});exit;
      //            $result = array_reverse($result);

      ?>
      <table id="example" class="table table-striped table-bordered" style="width:100%">
        <thead>
          <tr>
            <th>Caller<sup><i class="fas fa-info-circle" title="Incoming or Outgoing"></i></sup></th>
            <th>Broadcaster<sup><i class="fas fa-info-circle" title="Name Of The Other End User"></i></sup></th>
            <th>Rates<sup style="font-size: 8px;">per hour</sup></th>
            <th>Call Duration<sup style="font-size: 8px;">hour</sup></th>
            <th>Call Charges<sup style="font-size: 10px;">$</sup></th>

          </tr>
        </thead>
        <?php if (sizeof($result) != 0) {
          $size = sizeof($result);
          for ($i = 0; $i < sizeof($result); $i++) { ?>

            <tr>
              <td> <?php echo $result[$i]->{'caller_name'} ?> </td>
              <td><?php echo $result[$i]->{'receiver_name'} ?></td>
              <td><?php echo $result[$i]->{'receiver_hour_rate'} ?></td>
              <td><?php echo $result[$i]->{'call_duration'} ?></td>
              <td><?php echo $result[$i]->{'call_cost'} ?></td>
            </tr>
        <?php  }
        } ?>
        <thead>
          <tr>
            <th>Caller</th>
            <th>Broadcaster</th>
            <th>Rates</th>
            <th>Call Duration</th>
            <th>Call Charges</th>
        </thead>
      </table>
      <br><br><br><br><br>



    </div>
  </div>




  <script type="text/javascript" src="js/call_history.js"></script>
</body>

<script type="text/javascript">
  $(document).ready(function() {
    $('#example').DataTable();

  });
</script>

</html>