<?php
session_start(); // Starting Session

	if(isset($_SESSION["user"])==false)
	{
		header('Location:./index.php');
	}

    else
    {
		unset($_SESSION["user"]);
		header('Location:./index.php');
	}
	
?>